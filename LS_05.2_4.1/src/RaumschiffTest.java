
public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Raumschiff romulaner = new Raumschiff();
		Raumschiff klingonen = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
		romulaner.setEnergieversorgungInProzent(2);
		romulaner.setPhotonentorpedoAnzahl(100);
		romulaner.setSchildeInProzent(100);
		Ladung romulanerLadung1 = new Ladung();
		romulanerLadung1.setBezeichnung("Borg-Schrott");
		romulanerLadung1.setMenge(5);
		romulaner.addLadung(romulanerLadung1);
		
		Ladung romulanerLadung2 = new Ladung();
		romulanerLadung2.setBezeichnung("Rote Materie");
		romulanerLadung2.setMenge(2);
		romulaner.addLadung(romulanerLadung2);
		
		Ladung romulanerLadung3 = new Ladung();
		romulanerLadung3.setBezeichnung("Plasma-Waffe");
		romulanerLadung3.setMenge(50);
		
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setSchildeInProzent(100);
		
		Ladung klingonenLadung1 = new Ladung();
		
		klingonenLadung1.setBezeichnung("Ferengi Schneckensaft");
		klingonenLadung1.setMenge(200);
		klingonen.addLadung(klingonenLadung1);
		
		Ladung klingonenLadung2 = new Ladung();
		klingonenLadung2.setBezeichnung("Bat'leth Klingonen Schwert");
		klingonenLadung2.setMenge(200);
		klingonen.addLadung(klingonenLadung2);

		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setSchildeInProzent(80);
		Ladung vulkanierLadung1 = new Ladung();
		vulkanierLadung1.setBezeichnung("Forschungssonde");
		vulkanierLadung1.setMenge(35);
		vulkanier.addLadung(vulkanierLadung1);
		
		Ladung vulkanierLadung2 = new Ladung();
		vulkanierLadung2.setBezeichnung("Photonentorpedo");
		vulkanierLadung2.setMenge(3);
		vulkanier.addLadung(vulkanierLadung2);
	}
		

}
