import java.util.Scanner;
public class LS051_A3_2_Methoden_verwenden_und_ueben {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was moechten Sie bestellen?");
	

		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
public static String liesString(String text)
{Scanner myScanner = new Scanner (System.in);
System.out.println(text);
String printWert = myScanner.next();
return printWert;
}
public static int liesInt(String text)
{Scanner myScanner = new Scanner (System.in);
System.out.println(text);
int printWert = myScanner.nextInt();
return printWert;
}
public static double liesDouble(String text)
{Scanner myScanner = new Scanner(System.in);
System.out.println(text);
double printWert = myScanner.nextDouble();
return printWert;
}
public static double berechneGesamtnettopreis(int anzahl, double
preis)
{double erg = anzahl + preis; 
return erg;

}
public static double berechneGesamtnettopreis(double nettogesamtpreis, 
double mwst) {double erg = nettogesamtpreis * (1 + mwst / 100);
return erg;
}
}
