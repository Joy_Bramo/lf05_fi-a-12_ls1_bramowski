﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		while (true) {
			Scanner tastatur = new Scanner(System.in);

			double zuZahlenderBetrag1 = fahrkartenbestellungErfassen();
			// Geldeinwurf
			// -----------
			double Gesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag1);

			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();
			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			double rückgabebetrag1 = rueckgeldAusgeben(Gesamtbetrag, zuZahlenderBetrag1);
		}
	}

	public static double fahrkartenbestellungErfassen() {

		double ticketAnzahl;
		Scanner tastatur = new Scanner(System.in);

		
		
		int zuZahlenderBetrag1 = 0;
		double ticketPreis = 0.0;
		
		while(true) {
//			System.out.println("Fahrkartenbestellvorgang:\n" + "=========================\n" + "\n"
//					+ "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
//					+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
//					+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
//			System.out.print("Ihre Wahl : ");
			
			
			
			
			double[] fahrkartenpreise;
			fahrkartenpreise = new double[] {
			0.0,2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90		
			};
			String[] fahrkartenBezeichnung;
			fahrkartenBezeichnung = new String[] {"ja",
					"Einzelfahrschein Berlin AB ","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC"
					,"Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC",
					"Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"
			};
//			System.out.println(fahrkartenpreise[zuZahlenderBetrag1] +" " +fahrkartenBezeichnung[zuZahlenderBetrag1]);
			for(int i =1;i<fahrkartenpreise.length;i++) {
				System.out.printf("%s [%.2f EURO] (%d)\n", fahrkartenBezeichnung[i],fahrkartenpreise[i],i);
			}

				System.out.println("Bitte auwählen:");
				
				zuZahlenderBetrag1 = tastatur.nextInt();
//			if (zuZahlenderBetrag1 == 1) {
//				ticketPreis += 2.90;
//				break;
//
//			} else if (zuZahlenderBetrag1 == 2) {
//				ticketPreis += 8.60;
//				break;
//			} else if (zuZahlenderBetrag1 == 3) {
//				ticketPreis += 23.50;
//				break;
//			} else  {
//			System.out.println("Ist ein ungültiger Wert, passenden Wert eingeben ");
//			
//		}
//		}
//
		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextInt();
		if (ticketAnzahl <= 0 || ticketAnzahl > 10) {
			System.out.println("Der Wert ist ungültig die Anzahl wird auf 1 gesetzt");
			ticketAnzahl = 1;
		}
		double preis = fahrkartenpreise[zuZahlenderBetrag1] *= ticketAnzahl;
		return preis;}
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag1) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag1) {
			System.out.printf("Noch zu zahlen: %.2f%s\n", (zuZahlenderBetrag1 - eingezahlterGesamtbetrag), " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double Gesamtbetrag, double zuZahlenderBetrag1) {
		double rückgabebetrag;

		rückgabebetrag = Gesamtbetrag - zuZahlenderBetrag1;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "Euro");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
		return rückgabebetrag;
	}

	public static void warte(int millisekunden) {
		System.out.print("=");
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + "" + einheit);
	}

}
